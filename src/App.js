import './App.css';
import { ArrayDataBinding } from './component/array-data-binding';
import { PrimitiveDataBinding } from './component/data-binding-object';
import { ObjectDataBinding } from './component/data-binding-primitive-type';
import { NestedObjectArrayDataBinding } from './component/nested-object-array-data-binding';
import { ObjectArrayBinding } from './component/object-array-data-binding';

function App() {
  return (
    <div className="App">
       <PrimitiveDataBinding />
       <hr/>
       <ObjectDataBinding />
       <hr/>
       <ArrayDataBinding />
       <hr/>
       <ObjectArrayBinding />
       <hr/>
       <NestedObjectArrayDataBinding />
    </div>
  );
}

export default App;
