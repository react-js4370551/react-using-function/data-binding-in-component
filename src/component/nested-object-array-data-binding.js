
export const NestedObjectArrayDataBinding = ()=>{
    const data = [
        {'Category':'Electroins', 'products':['Mobile', 'Laptop']},
        {'Category':'Footwaer', 'products':['Sports', 'Boot', 'Formaml']}
    ]

    return (
        <div>
            <h3>Menu</h3>
            <ol>
                {
                    data.map((item) => 
                        <li>
                            {item.Category}
                            <ul>
                                {
                                    item.products.map((product)=> <li>{product}</li>)
                                }
                            </ul>
                        </li>
                    )
                }
            </ol>
        </div>
    );
}