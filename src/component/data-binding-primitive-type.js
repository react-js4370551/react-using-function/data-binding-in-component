export const ObjectDataBinding =()=>{
    let product = {
        id : 1,
        name : 'iphone',
        price : 123456
    }

    return(
        <div>
            <dl>
                <dt>
                    id
                </dt>
                <dd>
                    {product.id}
                </dd>
                <dt>
                    name
                </dt>
                <dd>
                    {product.name}
                </dd>
                <dt>
                    price
                </dt>
                <dd>
                    {product.price}
                </dd>
            </dl>
        </div>
    );
}