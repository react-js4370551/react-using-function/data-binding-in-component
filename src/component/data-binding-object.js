//to bind data in react we use {}.
export function PrimitiveDataBinding() {

    let id = 1;
    let name = 'Iphone';
    let price = 12345.0;
    return (
        <div>
            <dl>
                <dt>
                    id
                </dt>
                <dd>
                    {id}
                </dd>
                <dt>
                    name
                </dt>
                <dd>
                    {name}
                </dd>
                <dt>
                    price
                </dt>
                <dd>
                    {price}
                </dd>
            </dl>
        </div>
    );
}