export const ObjectArrayBinding = () => {

    const products = [
        { 'id': 1, 'name': 'iphone', 'price': 1234, 'inStock': true },
        { 'id': 2, 'name': 'laptop', 'price': 3421, 'inStock': false }
    ]

    return (
        <div>
            <table>
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Price</th>
                        <th>Stock</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        products.map((product) => {
                            return (
                                <tr>
                                    <td>{product.id}</td>
                                    <td>{product.name}</td>
                                    <td>{product.price}</td>
                                    <td>{product.inStock ? 'Available' : 'OutOfStock'}</td>
                                </tr>
                            )
                        })

                        // OR
                        //products.map((product) => 
                        //         <tr>
                        //             <td>{product.id}</td>
                        //             <td>{product.name}</td>
                        //             <td>{product.price}</td>
                        //             <td>{product.inStock ? 'Available' : 'OutOfStock'}</td>
                        //         </tr>
                        // )

                        // OR
                        // products.map((product) => 
                        //      (
                        //         <tr>
                        //             <td>{product.id}</td>
                        //             <td>{product.name}</td>
                        //             <td>{product.price}</td>
                        //             <td>{product.inStock ? 'Available' : 'OutOfStock'}</td>
                        //         </tr>
                        //     )
                        // )
                    }
                </tbody>
            </table>
        </div>
    );
}